set -x
mqtt_name="mqtt"

echo "press CTRL+C to cancel"

trap terminate SIGINT
terminate(){
	echo "exiting MQTT demo"
	set -x
	pkill -SIGINT -P $$
	echo "done"
	exit
}

log () {
	sed -e "s/^/[$1]: /" 
}

topic='mqttnet/samples/topic/2'

docker rm -f $mqtt_name &> /dev/null

sleep 1;
{ docker run  --name $mqtt_name -p 1883:1883 -p 9001:9001 -v "$(pwd)/mosquitto.conf":/mosquitto/config/mosquitto.conf   eclipse-mosquitto |& log "MQTT_BROKER"; }  &

base_dir="$(pwd)"
sleep 1;
{ cd c_sharp && dotnet run  |& log "C_SHARP_SUB"; } &
cd $base_dir

sleep 2;
{ docker exec $mqtt_name mosquitto_sub -t $topic  |& log "MQ_CLI_CLIENT_SUB1"; } &
sleep 2;
{ docker exec $mqtt_name mosquitto_pub -t $topic  -m "dewi test message" |& log "MQ_CLI_CLIENT_PUB2"; } &
sleep 6;
{ docker exec $mqtt_name mosquitto_pub -t "$topic"  -m "dewi test messag2" |& log "MQ_CLI_CLIENT_PUB2"; } &

wait
