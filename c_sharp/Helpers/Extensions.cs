using System.Text.Json;

namespace Helpers;

internal static class Extensions
{
    public static TObject DumpToConsole<TObject>(this TObject @object)
    {
        Console.WriteLine($"-----------");
        Console.WriteLine($"{ @object.ToString() }");
        var output = "NULL";
        if (@object != null)
        {
            output = JsonSerializer.Serialize(@object, new JsonSerializerOptions
            {
                WriteIndented = true
            });
        }
        
        Console.WriteLine($"[{@object?.GetType().Name}]:\r\n{output}");
        return @object;
    }
}
