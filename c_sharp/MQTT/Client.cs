// Licensed to the .NET Foundation under one or more agreements.
// The .NET Foundation licenses this file to you under the MIT license.
// See the LICENSE file in the project root for more information.

// ReSharper disable UnusedType.Global
// ReSharper disable UnusedMember.Global
// ReSharper disable InconsistentNaming

using MQTTnet.Client;
using MQTTnet;
using Helpers;
using System;

namespace MQTT;


public static class Client
{

      public static async Task Handle_Received_Application_Message()
      {
          /*
          * This sample subscribes to a topic and processes the received message.
          */

          var mqttFactory = new MqttFactory();

          using (var mqttClient = mqttFactory.CreateMqttClient())
          {
              var mqttClientOptions = new MqttClientOptionsBuilder()
                  .WithTcpServer("localhost", 1883)
                  .Build();

              Console.WriteLine("Connecting....");

              // Setup message handling before connecting so that queued messages
              // are also handled properly. When there is no event handler attached all
              // received messages get lost.
              mqttClient.ApplicationMessageReceivedAsync += e =>
              {
                  Console.WriteLine("Received application message.");
                  e.ApplicationMessage.ConvertPayloadToString().DumpToConsole();

                  return Task.CompletedTask;
              };

              await mqttClient.ConnectAsync(mqttClientOptions, CancellationToken.None);

              var mqttSubscribeOptions = mqttFactory.CreateSubscribeOptionsBuilder()
                  .WithTopicFilter(f => { f.WithTopic("mqttnet/samples/topic/2"); })
                  .Build();

              await mqttClient.SubscribeAsync(mqttSubscribeOptions, CancellationToken.None);


              Console.WriteLine("MQTT client subscribed to topic.");

              var cancelMessage = "Press the x key to cancel";

              Console.WriteLine(cancelMessage);

              Task cancelTask = Task.Run(() =>
              {
                  while ( !Console.ReadKey().KeyChar.Equals('x')  )
                  {
                      Console.WriteLine(cancelMessage);
                  }

                  Console.WriteLine("EXITING");
              });


              await Task.Delay(Timeout.Infinite);

              Console.WriteLine("EXITING");

          }
      }

}

